//
//  ViewControllerWeb.swift
//  PR3S
//
//  Created by Javier Salvador Calvo on 12/11/16.
//  Copyright © 2016 UOC. All rights reserved.
//

import UIKit

import WebKit

import Speech

class ViewControllerWeb: UIViewController, UIWebViewDelegate, WKNavigationDelegate, WKUIDelegate, SFSpeechRecognizerDelegate {

    
    @IBOutlet weak var webView: WKWebView!
    var m_str_json:String = ""
    var m_downloadManager:ThreadDownloadManager = ThreadDownloadManager()
    
    // BEGIN-CODE-UOC-9
    let speechRecognizer: SFSpeechRecognizer? = SFSpeechRecognizer(locale: Locale(identifier: "en-US"))
    var recognitionRequest: SFSpeechAudioBufferRecognitionRequest? = nil
    var recognitionTask: SFSpeechRecognitionTask? = nil
    let audioEngine = AVAudioEngine()
    var inputNode: AVAudioInputNode? = nil
    // END-CODE-UOC-9
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // BEGIN-CODE-UOC-2
        self.webView? = WKWebView(frame: .zero, configuration: WKWebViewConfiguration())
        
        // Assign delegates
        self.webView?.navigationDelegate = self
        self.webView?.uiDelegate = self
        
        // Disable scrolling, page bounces, navigation gestures and fit the content to give an native UX
        self.webView?.scrollView.isScrollEnabled = false
        self.webView?.scrollView.bounces = false
        self.webView?.allowsBackForwardNavigationGestures = false
        self.webView?.contentMode = .scaleToFill
        
        if let path = Bundle.main.path(forResource: "index", ofType: "html", inDirectory: "www") {
            let url = URL(fileURLWithPath: path)
            self.webView?.load( URLRequest(url: url) )
        }
        
        view = self.webView
        // END-CODE-UOC-2
        
        
        self.m_downloadManager.m_main = self
        self.m_downloadManager.Start()
        
        
        // BEGIN-CODE-UOC-3        
        // Encode the received data in UTF-8
        var stringUrls:[String]?
        
        do {
            if let receivedData = m_str_json.data(using: String.Encoding.utf8) {
                // Parse the received data into a String array
                stringUrls = try JSONSerialization.jsonObject(with: receivedData, options: .mutableContainers) as? [String]
                print("From the received data were the following URLs parsed: \(stringUrls!)")
            } else {
                print("Error no data received")
            }
        } catch {
            print("Error deserializing JSON data: \(error)")
        }
        
        // Call the downloader manager for each url
        if let urls = stringUrls {
            for url in urls {
                self.m_downloadManager.Download(url: url)
            }
        }

//  ******    QÜESTIONS    ******
//  * Des del punt de vista de la programació concurrent quina diferència hi ha entre els mètodes Download i DownloadInternal de ThreadDownloadManager?
//
//  En el mètode "Download" es força ha través de la funció "perform" a un thread concret (el generat en la funció "Start" i del que se’n guarda una referència en la variable "m_thread") d'executar una instrucció concreta (executar el mètode "DownloadInternal"). La execució del mètode "DownloadInternal" es realitzarà llavors de manera asíncrona y paralel.la als altres threads. En el mètode "DownloadInternal", en canvi, totes les seves ordres son executades pel mateix thread que el crida, el qual serà definit en el temps d’execució. La execució de les ordres serà en aquest cas de manera síncrona i concurrent amb els altres threads.
//
//  * Dins de BEGIN-CODE-UOC-3 , END-CODE-UOC-3 realitzem múltiples crides al mètode Download de ThreadDownloadManager. En què es transformen aquestes crides quan es realitza un perform(#selector(DownloadInternal)?
//
//  Cadascuna de les crides al mètode "Download" es transformarà en un esdeveniment, que s’afegirà a la cua d'esdeveniments del thread creat en la funció "Start" i del que se’n guarda una referencia a "m_thread". Aquests esdeveniments s’executaran de manera seqüencial un darrera l'altre.
//  *****************************
        // END-CODE-UOC-3
        
        
        // BEGIN-CODE-UOC-10
        checkSpeechRecognitionAuthorization()
        speechRecognizer?.delegate = self

        let audioSession = AVAudioSession.sharedInstance()
        
        do {
            try audioSession.setCategory(AVAudioSession.Category.record)
            try audioSession.setMode(AVAudioSession.Mode.measurement)
            try audioSession.setActive(true)
        } catch {
            print(error)
        }
        
        inputNode = audioEngine.inputNode
        
        let recordingFormat = inputNode?.outputFormat(forBus: 0)
        inputNode?.installTap(onBus: 0, bufferSize: 2048, format: recordingFormat) {
            (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
            self.recognitionRequest?.append(buffer)
        }
        audioEngine.prepare()
        
        do {
            try audioEngine.start()
        } catch {
            print(error)
        }
        
        startRecording()
        // END-CODE-UOC-10
        
    }
    
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: (WKNavigationActionPolicy) -> Void) {
        
        // BEGIN-CODE-UOC-6
        let url: URL = navigationAction.request.url!
        
        if (url.scheme?.lowercased() == "theorganization") {
            
            guard let function: String = url.host else {
                return
            }
            
            switch function {
            case "Play":
                print("Triggered a \"play\" action from the html")
                self.m_downloadManager.Play()
            case "Pause":
                print("Triggered a \"pause\" action from the html")
                self.m_downloadManager.Pause()
            default:
                break
            }
            decisionHandler(.cancel)
        } else {
            decisionHandler(.allow)
        }
//  ******    QÜESTIONS    ******
//  * En HTML existeix un esdeveniment (event) anomenat OnLoad que s'executa quan la pàgina està carregada. Què faríem perquè comencés l'execució del m_downloadManager quan es carregués la interfície? On mouríem unes poques línies del codi actual?
//
//  Per que la execució del "m_downloadManager" comences un cop la interfície web estigués completament carregada s'haurien de realitzar les crides al mètode "Download" amb les URLs dels arxius a descarregar dins del mètode "webView" un cop capturat l'esdeveniment "OnLoad".
//  *****************************
         // END-CODE-UOC-6
        
        
        
        
    }

    // BEGIN-CODE-UOC-5
    @objc func IncProgress()
    {
        print("Executing the method \"IncProgress\" inside index.html")
        self.webView?.evaluateJavaScript("IncProgress()", completionHandler: {(result, error) in
            if let errorFromHtml = error {
                print("Error: the method IncProgress() throws the error '\(errorFromHtml)'")
            }
        })
    }
    
    @objc func AddLog(msg:String)
    {
        print("Executing the method \"AddLog\" inside index.html with the parameter \"\(msg)\"")
        self.webView?.evaluateJavaScript("AddLog('\(msg)')", completionHandler: {(result, error) in
            if let errorFromHtml = error {
                print("Error: the method AddLog('\(msg)') throws the error '\(errorFromHtml)'")
            }
        })
    }
    // END-CODE-UOC-5
    
    // BEGIN-CODE-UOC-11
    func checkSpeechRecognitionAuthorization() {
        SFSpeechRecognizer.requestAuthorization { authStatus in
            OperationQueue.main.addOperation {
                switch authStatus {
                case .authorized:
                    print("Acces to speech recognition was accepted")
                case .denied:
                    print("User denied access to speech recognition")
                case .restricted:
                    print("Speech recognition restricted on this device")
                case .notDetermined:
                    print("Speech recognition not yet authorized")
                default:
                    break
                }
            }
        }
    }
    
    func startRecording() {
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        recognitionRequest?.shouldReportPartialResults = true
        
        recognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequest!) { result, error in
            var isFinal = false
            if let result = result {
                if let text = result.bestTranscription.segments.last?.substring {
                    print("The speech recognition engine catches the word: \"\(text)\"")
                    switch text.lowercased() {
                    case "play":
                        print("Triggered a \"play\" action from the speech recognizer")
                        self.m_downloadManager.Play()
                    case "stop":
                        print("Triggered a \"pause\" action from the speech recognizer")
                        self.m_downloadManager.Pause()
                    case "deactivate":
                        self.deactivateSpeechRecognition()
                        print("Speech recognizer deactivated")
                    default:
                        break
                    }
                    isFinal = result.isFinal
                }
            }
            
            if isFinal {
                self.recognitionRequest?.endAudio()
                self.recognitionTask?.finish()
                print("Previous recognition task ends, starting a new one")
                self.startRecording()
            }
            
            if error != nil {
                print(error!)
                self.deactivateSpeechRecognition()
            }
        }
    }
    
    func deactivateSpeechRecognition() {
        self.audioEngine.stop()
        self.inputNode?.removeTap(onBus: 0)
        self.recognitionRequest = nil
        self.recognitionTask = nil
    }
    // END-CODE-UOC-11
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

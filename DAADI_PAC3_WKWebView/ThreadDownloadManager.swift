//
//  ThreadDownloadManager.swift
//  PR3S
//
//  Created by Javier Salvador Calvo on 12/11/16.
//  Copyright © 2016 UOC. All rights reserved.
//

import UIKit

class ThreadDownloadManager: NSObject {

    
    var m_thread:Thread? = nil
    var theLock:NSLock = NSLock()
    var m_main:ViewControllerWeb? = nil
    var m_pause:Bool = false
    var m_file_list:[String] = []
    
    
    func Start()
    {
    
        self.m_thread = Thread(target: self, selector: #selector(ThreadDownloadManager.ThreadMainLoop(_:)), object: nil)
        self.m_thread?.start()
    }
    
    
    
    func Download(url:String)
    {
        self.perform(#selector(DownloadInternal), on: self.m_thread!, with: url, waitUntilDone: false)
    }
    
    @objc func DownloadInternal(url:String)
    {
        // BEGIN-CODE-UOC-4
        m_file_list.append(url)
        // m_file_list.count>=1
        if(m_file_list.count==1){
        DownloadSerialized(url:url)
        }
    }
        
    func DownloadSerialized(url: String)
    {
        // Create URL from String
        guard let fileUrl = URL(string: url) else {
            print("Error: cannot create a URL from the given string")
            return
        }
        
        print("Downloading file from \(url) in thread \(Thread.current))")
        
        
        let basePath:String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let fileManager:FileManager = FileManager()
        do {
            try fileManager.createDirectory(atPath: basePath, withIntermediateDirectories: true, attributes: nil)
        } catch {
            print("Error: cannot create a directory to save the file - " + error.localizedDescription)
            return
        }
        
        let urlRequest: URLRequest = URLRequest(url: fileUrl, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60)
        
        let defaultSession:URLSession = URLSession.shared
        
        var localPath: String = ""
        
        let dataTask:URLSessionDataTask = defaultSession.dataTask(with: urlRequest) {
            (data, response, error) in
            
            guard error == nil else {
                print("Error: cannot obtain a file from the url '\(url)' - ", error!)
                return
            }
            
            guard let receivedData = data else {
                print("Error: no data received from the url '\(url)'")
                return
            }
            
            let index = url.lastIndex(of:"/") ?? url.endIndex
            let m_file_name = url[index..<url.endIndex]
            
            do {
                localPath = basePath + m_file_name
                print("local Path: %", localPath)
                if (fileManager.fileExists(atPath: localPath)){
                    try fileManager.removeItem(atPath: localPath)
                }
                fileManager.createFile(atPath: localPath, contents: receivedData, attributes: [FileAttributeKey.protectionKey : FileProtectionType.none])
                
                print("Downloaded file with name: '\(fileUrl.lastPathComponent)'")
                
                Thread.sleep(forTimeInterval: 5)
                
                self.m_main?.performSelector(onMainThread: #selector(ViewControllerWeb.AddLog), with: localPath, waitUntilDone: false)
                self.m_main?.performSelector(onMainThread: #selector(ViewControllerWeb.IncProgress), with: nil, waitUntilDone: false)
                
                self.perform(#selector(self.Next), on: self.m_thread!, with: nil, waitUntilDone: false)
                
            } catch { }
        }
        dataTask.resume()
    }
    
    @objc func Next()
    {
        // END-CODE-UOC-4
        
        
        
        // BEGIN-CODE-UOC-8
        while (IsPause()) {
            Thread.sleep(forTimeInterval: 3)
        }
        m_file_list.remove(at: 0)
        
        if (m_file_list.count>0) {
            let url = m_file_list[0]
            DownloadSerialized(url: url)
        }
        
//  ******    QÜESTIONS    ******
//  * Imagina que després de la descàrrega de cada element el ThreadDownloadManager realitza un càlcul complex amb cada element. Per exemple que en el cas dels textos realitza una anàlisi per identificar de què parla el text. Què hauria passat si en lloc d'usar un únic Thread per realitzar la descàrrega i l'anàlisi haguéssim optat per crear un Thread per a cada petició de descàrrega sense usar cap tècnica de seqüenciació o ordenació de la descàrrega?
//
//  En el cas d'utilitzar un thread diferent per a cada petició de descàrrega i no utilitzant cap tècnica de seqüenciació o ordenació de tot el procés general de descàrrega, correríem el risc d'arribar a saturar els recursos del dispositiu (capacitat de càlcul del processador, memòria RAM disponible, accés a la memòria interna, ...) al executar processos que poden requerir l'ús intensiu d'aquests. Com a conseqüència de la saturació podríem generar des de lentitud en la resposta de la interfície gràfica, fins al bloqueig total del funcionament de la aplicació o a que el sistema operatiu (iOS) decideixi matar els processos sobre els que s’executa la App.
//  *****************************
        // END-CODE-UOC-8
        
    }
    
    
    
    // BEGIN-CODE-UOC-7
    func Play()
    {
        self.theLock.lock()
        self.m_pause = false
        self.theLock.unlock()
    }
    

    func Pause()
    {
        self.theLock.lock()
        self.m_pause = true
        self.theLock.unlock()
    }
    
    func IsPause() -> Bool
    {
        self.theLock.lock()
        let pauseStatus = m_pause
        self.theLock.unlock()
        return pauseStatus
    }
    // END-CODE-UOC-7
    
    
    @objc func Message(msg:String)
    {
        
        
    }

    
    @objc func ThreadMainLoop(_:Any)
    {
        self.perform(#selector(Message), on: self.m_thread!, with: "Start", waitUntilDone: false)
        CFRunLoopRun();
    }

}
